<?php

class room_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = "room";

    }

    public function get_all()
    {
        $results = $this->db->get($this->table)->result();
        return $results;
    }

    public function join()
    {
        $this->db->select('*');
        $this->db->from('room_image');


        $this->db->join('room', 'room_image.room_id = room.id');
        $query = $this->db->get();

        return $query->result();
    }

    public function get($where = array())
    {
        $results = $this->db->where($where)->get($this->table)->row();
        return $results;
    }


    public function update($where = array(), $data = array())
    {
        $update = $this->db->where($where)->update($this->table, $data);
        return $update;
    }

    public function delete($where = array())
    {
        $delete = $this->db->where($where)->delete($this->table);
        return $delete;
    }

    public function insert($data = array())
    {
        $insert = $this->db->insert($this->table, $data);
        return $insert;
    }
}

?>