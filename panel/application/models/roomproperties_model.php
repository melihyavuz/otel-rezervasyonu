<?php

class roomproperties_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = "room_properties";

    }

    public function get_all()
    {
        $results = $this->db->get($this->table)->result();
        return $results;
    }

    public function get($where = array())
    {


        $results = $this->db->where($where)->get($this->table)->row();

        if(!empty($results)){
            $title=$results->title.",";
        }
        else{
            return false;
        }
        return $title;

    }


    public function update($where = array(), $data = array())
    {
        $update = $this->db->where($where)->update($this->table, $data);
        return $update;
    }

    public function delete($where = array())
    {
        $delete = $this->db->where($where)->delete($this->table);
        return $delete;
    }

    public function insert($data = array())
    {
        $insert = $this->db->insert($this->table, $data);
        return $insert;
    }
}

?>