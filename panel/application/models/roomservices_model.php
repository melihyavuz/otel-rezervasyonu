<?php

class Roomservices_model extends CI_Model
{
  public function __construct()
  {
      parent::__construct();
      $this->table="room_extra_services";
  }
  public function get_all(){
    return  $this->db->get($this->table)->result();
  }
    public function update($where=array(),$data=array()){
        return  $this->db->where($where)->update($this->table,$data);

    }

}