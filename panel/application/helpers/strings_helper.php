<?php
function getMessage($key)
{
    $messages = array(
        "success" => "İşlem Başarılıdır",
        "isActiveSuccess" => "Aktifleşti",
        "isActiveError" => "Pasifleşti",
        "CategoryNameError" =>"Kategori Adı Boş Bırakılamaz",
        "RoomCodeError" =>"Oda Numarası Boş Bırakılamaz",
        "RoomTitleError" =>"Oda Başlığı Boş Bırakılamaz",
        "RoomDetailError" =>"Oda Detayı Boş Bırakılamaz",
        "RoomSizeError" =>"Oda Boyutu Boş Bırakılamaz",
        "RoomPriceError" =>"Oda Fiyatı Boş Bırakılamaz",
        "RoomCapacityError" =>"Oda Kapasitesi Boş Bırakılamaz",
        "RoomPropertiesError" =>"Oda Özellikleri Boş Bırakılamaz",
        "RoomServisError" =>"Oda Ekstra Servisleri Boş Bırakılamaz",
        "AllFieldError" =>"Lütfen Tüm Alanları Doldurun",
        "PropertiesNameError" =>"Özellik Adı Boş Bırakılamaz",
    );
    return $messages[$key];
}