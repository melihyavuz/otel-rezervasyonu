<?php
function get_room_category()
{
    $CI =& get_instance();
    $CI->load->model("roomcategory_model");
    return $CI->roomcategory_model->get_all();
}

function get_room_properties()
{
    $CI =& get_instance();
    $CI->load->model("roomproperties_model");
    return $CI->roomproperties_model->get_all();
}
function get_room_category_id($id){
    $CI=&get_instance();
    $CI->load->model("roomcategory_model");
    $result=$CI->roomcategory_model->get(array("id"=>$id));
    return $result;

}

function get_room_properties_id($id)
{
    $CI =& get_instance();
    $CI->load->model("roomproperties_model");
    $id = explode(",", $id);
    $result = '';
    $title="<span style='color: red;'>Özellik Bulunamadı!!!</span>";
    foreach ($id as $key2) {

        $result .= $CI->roomproperties_model->get(array("id" => $key2, "isActive" => 1));



    }
    if(empty($result)){
        return $title;
    }
    $result = rtrim($result, ',');
    $result = ltrim($result, ',');

    return $result;


}

function get_day($day)
{
    $days = array(
        "Mon" => "Pzt",
        "Tue" => "Sal",
        "Wed" => "Çar",
        "Thu" => "Per",
        "Fri" => "Cum",
        "Sat" => "Cmt",
        "Sun" => "Paz"
    );
    return $days[$day];
}

function get_month($month)
{
    $months = array(
        "Jan" => "Oca",
        "Feb" => "Sub",
        "Mar" => "Mar",
        "Apr" => "Nis",
        "May" => "May",
        "Jun" => "Haz",
        "Jul" => "Tem",
        "Aug" => "Aug",
        "Sep" => "Eyl",
        "Oct" => "Eki",
        "Nov" => "Kas",
        "Dec" => "Ara"

    );
    return $months[$month];
}

?>