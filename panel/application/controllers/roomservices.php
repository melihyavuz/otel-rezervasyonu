<?php

class Roomservices extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("roomservices_model");
    }

    public function index()
    {
        $categories = $this->roomservices_model->get_all();
        $viewData=array(
            "categories"=>$categories
        );
        $this->load->view("roomservices",$viewData);
    }
    public function isActiveSetter(){
        $isActive=$this->input->post("isActive");
        $isActive = ($isActive == "true") ? 1 : 0;

        $id=$this->input->post("id");

        $this->roomservices_model->update(array("id"=>$id),array("isActive"=>$isActive));

    }

}