<?php

class Roomcategory extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("roomcategory_model");
    }

    public function index()
    {

        $categories = $this->roomcategory_model->get_all(array(), "rank ASC");
        $viewData = array(
            "categories" => $categories
        );
        $this->load->view("roomcategory", $viewData);
    }

    function isActiveSetter()
    {
        $id = $this->input->post("id");
        $isActive = $this->input->post("isActive");
        $isActive = ($isActive == "true") ? 1 : 0;
        $this->roomcategory_model->update(array("id" => $id), array("isActive" => $isActive));

    }

    public function newPage()
    {

        $this->load->view("new_roomcategory");
    }


    public function add()
    {
        $title = $this->input->post("title");

        $data = array(
            "title" => $title,
            "isActive" => 0
        );
        if (!empty($title)) {
            $insert = $this->roomcategory_model->insert($data);

            if ($insert) {
                $this->session->set_userdata(array(
                        "alert" => true,
                        "alert-message" => getMessage("success"),
                        "alert-type" => "success"
                    )
                );
                redirect(base_url("roomcategory"));
            } else {
                echo "Hata";
            }
        }
        else {

            $this->session->set_userdata(array(
                    "alert" => true,
                    "alert-message" => getMessage("CategoryNameError"),
                    "alert-type" => "error"
                )
            );
            redirect(base_url("roomcategory/newPage"));
        }
    }

    public function delete($id)
    {
        $delete = $this->roomcategory_model->delete(array("id" => $id));
        if ($delete) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"
            ));
            redirect(base_url("roomcategory"));
        } else {
            echo "Hata";

        }
    }

    public function editPage($id)
    {
        $edit = $this->roomcategory_model->get(array("id" => $id));
        $viewData = array(
            "edit" => $edit
        );
        $this->load->view("edit_roomcategory", $viewData);
    }

    public function edit($id)
    {

        $title = $this->input->post("title");

        $data = array(
            "title" => $title
        );
        $update = $this->roomcategory_model->update(array("id" => $id), $data);

        if ($update) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"
            ));
            redirect(base_url("roomcategory"));
        } else {
            echo "Hata";
        }
    }

    /**
     * @return mixed
     */
    public function rankOrder()
    {
        parse_str($this->input->post("data"), $data);

        $items = $data["sortId"];

        foreach ($items as $rank => $id) {
            $this->roomcategory_model->update(
                array(
                    "id" => $id,
                    "rank !=" => $rank
                ),
                array(
                    "rank" => $rank
                )

            );
        }


    }
}