<?php

class Roomproperties extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("roomproperties_model");
    }

    public function index()
    {
        $categories = $this->roomproperties_model->get_all();
        $viewData = array(
            "categories" => $categories
        );
        $this->load->view("roomproperties", $viewData);
    }

    function isActiveSetter()
    {
        $id = $this->input->post("id");
        $isActive = $this->input->post("isActive");
        $isActive = ($isActive == "true") ? 1 : 0;
        $this->roomproperties_model->update(array("id" => $id), array("isActive" => $isActive));
    }

    public function newPage()
    {
        $this->load->view("new_roomproperties");
    }

    public function add()
    {
        $title = $this->input->post("title");
        $properties = implode(",", $this->input->post("properties"));

        $data = array(
            "title" => $title
        );
        if(!empty($title)){
        $insert = $this->roomproperties_model->insert($data);
        if ($insert) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"
            ));
            redirect(base_url("roomproperties"));
        } else {
            echo "Hata";
        }
        }
        else{
            $this->session->set_userdata(array(
                "alert"=>true,
                "alert-message"=>getMessage("PropertiesNameError"),
                "alert-type"=>"error"

            ));
            redirect(base_url("roomproperties/newPage"));
        }
    }

    public function delete($id)
    {
        $delete = $this->roomproperties_model->delete(array("id" => $id));
        if ($delete) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"
            ));
            redirect(base_url("roomproperties"));
        } else {
            echo "Hata";
        }
    }

    public function editPage($id)
    {
        $edit = $this->roomproperties_model->get(array("id" => $id));
        $viewData = array(
            "edit" => $edit
        );
        $this->load->view("edit_roomproperties", $viewData);
    }

    public function edit($id)
    {

        $title = $this->input->post("title");

        $data = array(
            "title" => $title
        );
        $update = $this->roomproperties_model->update(array("id" => $id), $data);

        if ($update) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"
            ));
            redirect(base_url("roomproperties"));
        } else {
            echo "Hata";
        }
    }
}