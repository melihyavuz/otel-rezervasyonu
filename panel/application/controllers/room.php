<?php

class Room extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("room_model");
        $this->load->model("roomimage_model");
        $this->load->model("roomavailability_model");

    }

    public function index()
    {

        $categories = $this->room_model->get_all();
        $viewData = array(
            "categories" => $categories
        );
        $this->load->view("room", $viewData);
    }

    function isActiveSetter()
    {
        $id = $this->input->post("id");
        $isActive = $this->input->post("isActive");
        $isActive = ($isActive == "true") ? 1 : 0;
        $this->room_model->update(array("id" => $id), array("isActive" => $isActive));
    }

    public function newPage()
    {
        $this->load->view("new_room");
    }

    public function add()
    {
        $room_code = $this->input->post("room_code");
        $title = $this->input->post("title");
        $detail = $this->input->post("detail");
        $size = $this->input->post("size");
        $default_price = $this->input->post("default_price");
        $room_type_id = $this->input->post("room_type_id");
        $room_capacity = $this->input->post("room_capacity");
        $properties = implode(",", $this->input->post("properties"));
        $room_extra_services = implode(",", $this->input->post("extra_services"));

        $data = array(
            "room_code" => $room_code,
            "title" => $title,
            "detail" => $detail,
            "size" => $size,
            "default_price" => $default_price,
            "room_type_id" => $room_type_id,
            "room_capacity" => $room_capacity,
            "room_properties" => $properties,
            "room_extra_services" => $room_extra_services,
            "isActive" => 0

        );
        if($room_code&&$title&&$detail&&$size&&$default_price&&$room_type_id&&$room_capacity&&$properties&&$room_extra_services!="")
        {
            $insert = $this->room_model->insert($data);
        if ($insert) {
            $this->session->set_userdata(array(
                "alert"=>true,
                "alert-message"=>getMessage("success"),
                "alert-type"=>"success"

            ));
            redirect(base_url("room"));
        } else {
            echo "Hata";
        }
        }
        elseif(empty($room_code)){
            $this->session->set_userdata(array(
                "alert"=>true,
                "alert-message"=>getMessage("RoomCodeError"),
                "alert-type"=>"error"

            ));
            redirect(base_url("room/newPage"));
        }
        elseif(empty($title)){
            $this->session->set_userdata(array(
                "alert"=>true,
                "alert-message"=>getMessage("RoomTitleError"),
                "alert-type"=>"error"

            ));
            redirect(base_url("room/newPage"));
        }
        elseif(empty($detail)){
            $this->session->set_userdata(array(
                "alert"=>true,
                "alert-message"=>getMessage("RoomDetailError"),
                "alert-type"=>"error"

            ));
            redirect(base_url("room/newPage"));
        }
        elseif(empty($size)){
            $this->session->set_userdata(array(
                "alert"=>true,
                "alert-message"=>getMessage("RoomSizeError"),
                "alert-type"=>"error"

            ));
            redirect(base_url("room/newPage"));
        }
        elseif(empty($default_price)){
            $this->session->set_userdata(array(
                "alert"=>true,
                "alert-message"=>getMessage("RoomPriceError"),
                "alert-type"=>"error"

            ));
            redirect(base_url("room/newPage"));
        }
        elseif(empty($room_type_id)){
            $this->session->set_userdata(array(
                "alert"=>true,
                "alert-message"=>getMessage("CategoryNameError"),
                "alert-type"=>"error"

            ));
            redirect(base_url("room/newPage"));
        }
        elseif(empty($room_capacity)){
            $this->session->set_userdata(array(
                "alert"=>true,
                "alert-message"=>getMessage("RoomCapacityError"),
                "alert-type"=>"error"

            ));
            redirect(base_url("room/newPage"));
        }
        elseif(empty($properties)){
            $this->session->set_userdata(array(
                "alert"=>true,
                "alert-message"=>getMessage("RoomPropertiesError"),
                "alert-type"=>"error"

            ));
            redirect(base_url("room/newPage"));
        }
        elseif(empty($room_extra_services)){
            $this->session->set_userdata(array(
                "alert"=>true,
                "alert-message"=>getMessage("RoomServisError"),
                "alert-type"=>"error"

            ));
            redirect(base_url("room/newPage"));
        }
        else{
            $this->session->set_userdata(array(
                "alert"=>true,
                "alert-message"=>getMessage("AllFieldError"),
                "alert-type"=>"error"

            ));
            redirect(base_url("room/newPage"));

        }
    }

    public function delete($id)
    {
        $delete = $this->room_model->delete(array("id" => $id));
        if ($delete) {
            $this->session->set_userdata(array(
                "alert"=>true,
                "alert-message"=>getMessage("success"),
                "alert-type"=>"success"


            ));
            redirect(base_url("room"));
        } else {
            echo "Hata";
        }
    }

    public function editPage($id)
    {
        $edit = $this->room_model->get(array("id" => $id));
        $viewData = array(
            "edit" => $edit
        );
        $this->load->view("edit_room", $viewData);
    }

    public function edit($id)
    {

        $title = $this->input->post("title");

        $data = array(
            "title" => $title
        );
        $update = $this->room_model->update(array("id" => $id), $data);

        if ($update) {
            $this->session->set_userdata(array(
                "alert" => true,
                "alert-message" => getMessage("success"),
                "alert-type" => "success"
            ));
            redirect(base_url("room"));
        } else {
            echo "Hata";
        }
    }

    public function imageUpload($room_id)
    {
        $this->session->set_userdata("id", $room_id);
        $categories = $this->roomimage_model->get_all(array("room_id" => $room_id,), "rank ASC");
        $viewData = array(
            "categories" => $categories
        );
        $this->load->view("room_image", $viewData);

    }

    public function upload_image()
    {

        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name'] = true;
//        $config['max_size']             = 100;
//        $config['max_width']            = 1024;
//        $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors());

            print_r($error);
        } else {

            $room_id = $this->session->userdata("id");

            $_FILES = array('upload_data' => $this->upload->data());

            $img_id = $_FILES['upload_data']['file_name'];

            $this->roomimage_model->insert(array(
                "img_id" => $img_id,
                "room_id" => $room_id,
                "rank" => 0

            ));

        }
    }

    public function isActiveSetterForImage()
    {
        $id = $this->input->post("id");
        $isActive = $this->input->post("isActive");
        $isActive = ($isActive == "true") ? 1 : 0;
        $this->roomimage_model->update(array("id" => $id), array("isActive" => $isActive));
    }

    public function isCoverForImage()
    {
$cid=$this->input->post("cid");
        $id = $this->input->post("id");
        $isCover = $this->input->post("isCover");
        $isCover = ($isCover == "true") ? 1 : 0;
        $this->roomimage_model->update_cover(array("id" => $id), array("room_id"=> $cid), array("isCover" => $isCover));
    }

    public function rankOrder()
    {
        parse_str($this->input->post("data"), $data);

        $items = $data["sortId"];

        foreach ($items as $rank => $id) {
            $this->roomimage_model->update(
                array(
                    "id" => $id,
                    "rank !=" => $rank
                ),
                array(
                    "rank" => $rank
                )

            );
        }


    }

    public function delete_image($id)
    {
        $image = $this->roomimage_model->get(array("id" => $id));
        $file_name = FCPATH . "uploads/$image->img_id";
        if (unlink($file_name)) {
            $delete = $this->roomimage_model->delete(array("id" => $id));
            if ($delete) {
                redirect("room/imageUpload/$id");
            }

        }
    }

    public function newRoomAvailability($room_id)
    {
        $categories = $this->roomavailability_model->get(array(
            "room_id" => $room_id
        ));
        $availabilities = $this->roomavailability_model->get_all(array(
            "room_id" => $room_id,
            "daily_date >=" => date("Y-m-d")

        ));

        $viewData = array(
            "categories" => $categories,
            "availabilities" => $availabilities
        );
        $this->load->view("roomavailability", $viewData);
    }

    public function add_room_availability($room_id)
    {
        $availability = $this->input->post("date");
        $date = explode("-", $availability);

        $startDateArr = explode("/", $date[0]);
        $endDateArr = explode("/", $date[1]);

        $startDateStr = trim($startDateArr[2]) . "-" . trim($startDateArr[0]) . "-" . trim($startDateArr[1]);
        $endDateStr = trim($endDateArr[2]) . "-" . trim($endDateArr[0]) . "-" . trim($endDateArr[1]);
        $startDate = new DateTime($startDateStr);
        $finishDate = new DateTime(date("Y-m-d", strtotime("1 day", strtotime($endDateStr))));

        $interval = DateInterval::createFromDateString("1 day");
        $period = new DatePeriod($startDate, $interval, $finishDate);
        foreach ($period as $date) {
            $record_test = $this->roomavailability_model->get(
                array(
                    "room_id" => $room_id,
                    "daily_date" => $date->format("Y-m-d")
                )
            );

            if (empty($record_test)) {
                $this->roomavailability_model->insert(
                    array(
                        "daily_date" => $date->format("Y-m-d"),
                        "room_id" => $room_id,
                        "status" => 1
                    )
                );
            }
        }
        redirect(base_url("room/newRoomAvailability/$room_id"));


    }


}