<!doctype html>
<html lang="tr">
<head>
    <?php $this->load->view("includes/head") ?>
    <?php $this->load->view("roomproperties/list/page_style") ?>


</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php $this->load->view("includes/header") ?>
    <?php $this->load->view("includes/left_side_bar") ?>
    <div class="content-wrapper">
        <?php $this->load->view("roomproperties/list/breadcrumb") ?>
        <?php $this->load->view("roomproperties/list/main_content") ?>

    </div>

    <?php $this->load->view("includes/right_side_bar") ?>

</div>

<?php $this->load->view("includes/footer") ?>
<?php $this->load->view("roomproperties/list/page_script") ?>


</body>
</html>