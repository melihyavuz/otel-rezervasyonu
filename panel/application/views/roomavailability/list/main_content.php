<section class="content">
    <div class="row">
        <div class="col-md-8">
            <form method="post" action="<?php echo base_url("room/add_room_availability/$categories->room_id"); ?>">
                <div class="form-group">
                    <label>Date range:</label>

                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="date" class="form-control pull-right" id="reservation">
                    </div>
                    <!-- /.input group -->
                </div>
                <button type="submit" class="btn btn-primary">Oluştur</button>
            </form>

            <!-- /.box -->
        </div>

    </div>
    <div class="box box-primary mt-10">
        <ul class="availability_table">
            <?php foreach ($availabilities as $availability) { ?>
                <?php $date = new DateTime($availability->daily_date);
                $color = ($availability->status)?"green":"red"; ?>
                <li class="bg-<?php echo $color; ?>">
                    <?php echo $date->format("d") . "<br>" . get_month($date->format("M")) . "<br>" . get_day($date->format("D")); ?>

                </li>
            <?php } ?>

        </ul>

    </div>

</section>
