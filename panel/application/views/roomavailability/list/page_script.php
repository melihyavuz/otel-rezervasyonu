<script src="<?php echo base_url("assets"); ?>/dist/js/third_party/bootstrap-toggle.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/plugins/jQueryUI/jquery-ui.js"></script>
<script src="<?php echo base_url("assets"); ?>/dist/js/third_party/moment.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/plugins/daterangepicker/daterangepicker.js"></script>
<script>
    $(document).ready(function () {

        $('.toggle_check').bootstrapToggle();
        $('.toggle_check').change(function () {
            var isActive = $(this).prop('checked');
            var id = $(this).attr("dataID");
            var base_url = $('.base_url').text();
            $.post(
                base_url + "roomcategory/isActiveSetter", {id: id, isActive: isActive},
                function (response) {
                }
            )
        })
    })

</script>

<script>
    $(document).ready(function () {

        var base_url = $(".base_url").text();

        $("#sortable").sortable();
        $("#sortable").on("sortupdate", function () {
            var data = $(this).sortable("serialize");

            var postUrl = $(this).attr("postUrl");
            $.post(
                base_url + postUrl,
                {
                    data: data
                }
                ,

                function (response) {
                }
            );
        });


    });

</script>
<script>
    $(document).ready(function () {

        $('#reservation').daterangepicker();
    })

</script>