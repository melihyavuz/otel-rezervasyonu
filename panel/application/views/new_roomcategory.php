<!doctype html>
<html lang="tr">
<head>
    <?php $this->load->view("includes/head") ?>


</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php $this->load->view("includes/header") ?>
    <?php $this->load->view("includes/left_side_bar") ?>
    <div class="content-wrapper">
        <?php $this->load->view("roomcategory/add/breadcrumb") ?>
        <?php $this->load->view("roomcategory/add/main_content") ?>

    </div>

    <?php $this->load->view("includes/right_side_bar") ?>

</div>

<?php $this->load->view("includes/footer") ?>


</body>
</html>