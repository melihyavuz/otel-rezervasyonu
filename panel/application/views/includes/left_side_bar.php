
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url("assets");?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="<?php echo base_url("dashboard") ?>">
                    <i class="fa fa-th"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Oda İşlemleri</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo activate_menu('roomcategory'); ?>"><a href="<?php echo base_url("roomcategory") ?>"><i class="fa fa-circle-o"></i> Kategoriler</a></li>
                    <li class="<?php echo activate_menu('room'); ?>"><a href="<?php echo base_url("room") ?>"><i class="fa fa-circle-o"></i> Odalar</a></li>
                    <li class="<?php echo activate_menu('roomproperties'); ?>"><a href="<?php echo base_url("roomproperties") ?>"><i class="fa fa-circle-o"></i> Özellikler</a></li>
                    <li class="<?php echo activate_menu('roomservices') ?>"><a href="<?php echo base_url("roomservices")?>"><i class="fa fa-circle-o"></i> Ekstra Servisler</a></li>

                </ul>
            </li>










        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
