
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form action="<?php echo base_url("room/upload_image"); ?>" class="dropzone">
            </form>
        </div>
    </div>
    <div class="box">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr>
                    <th>Başlık</th>
                    <th>Ön Resim</th>

                    <th>Durum</th>
                    <th class="col-md-2">İşlemler</th>
                </tr>
                <tbody id="sortable" postUrl="room/rankOrder">
                <?php foreach ($categories as $category) { ?>

                    <tr id="sortId-<?php echo $category->id; ?>">
                        <td>
                            <a  data-lightbox="roadtrip"  href="<?php echo base_url("uploads") . "/" . $category->img_id; ?>" data-title="My caption">
                                <img
                                        width="80"
                                        src="<?php echo base_url("uploads") . "/" . $category->img_id; ?>"
                                        alt="<?php echo $category->img_id; ?>"
                                        class="img-responsive"
                                />
                            </a>

                        </td>
                        <td>
                            <input <?php echo ($category->isCover == 1) ? "checked" : ""; ?>
                                    dataID="<?php echo $category->id ?>"  dataCID="<?php echo $category->room_id ?>" class="toggle_cover"
                                    data-toggle="toggle" data-onstyle="success" data-offstyle="danger"
                                    data-on="Aktif" data-off="Pasif" data-size="mini" type="checkbox">
                        </td>
                        <td>
                            <input <?php echo ($category->isActive == 1) ? "checked" : ""; ?>
                                    dataID="<?php echo $category->id ?>" class="toggle_check"
                                    data-toggle="toggle" data-onstyle="success" data-offstyle="danger"
                                    data-on="Aktif" data-off="Pasif" data-size="mini" type="checkbox">
                        </td>

                        <td>

                            <a class="removeBtn"
                               dataURL="<?php echo base_url("room/delete_image/$category->id") ?>"> <i
                                        class="fa fa-fw fa-trash" style="font-size:16px;"></i></a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.box -->

</section>
<script src="<?php echo base_url("assets"); ?>/dist/js/third_party/lightbox.min.js"></script>

<script src="<?php echo base_url("assets"); ?>/dist/js/third_party/lightbox-plus-jquery.min.js"></script>