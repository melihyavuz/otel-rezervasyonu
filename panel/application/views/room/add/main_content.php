<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <div class="box box-primary">

                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="<?php echo base_url("room/add") ?>" method="POST">
                    <div class="box-body col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Oda Numarası*</label>
                            <input type="text" class="form-control" name="room_code">
                        </div>
                    </div>
                    <div class="box-body col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Başlık*</label>
                            <input type="text" class="form-control" name="title">
                        </div>
                    </div>
                    <div class="box-body col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Açıklama*</label>
                            <textarea id="detail" name="detail" rows="10" cols="80">
                                            This is my textarea to be replaced with CKEditor.
                    </textarea></div>
                    </div>
                    <div class="box-body col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Oda Boyutu*</label>
                            <input type="text" class="form-control" name="size">
                        </div>
                    </div>
                    <div class="box-body col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Fiyat(Gecelik)*</label>
                            <input type="text" class="form-control" name="default_price">
                        </div>
                    </div>
                    <div class="box-body col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Kategorisi*</label>
                            <select name="room_type_id" class="form-control">
                                <option value="">Oda Kategorisi Seçiniz...</option>
                                <?php foreach (get_room_category() as $category) { ?>
                                    <option value="<?php echo $category->id; ?>"><?php  echo $category->title; ?></option>
                                <?php } ?>
                            </select></div>
                    </div>
                    <div class="box-body col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Kapasite*</label>
                            <select name="room_capacity" class="form-control">
                                <option value="">Oda Kapasitesi Seçiniz...</option>

                                <?php for ($i = 1; $i <= 10; $i++) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo "$i Kişilik"; ?></option>
                                <?php } ?>
                            </select></div>
                    </div>
                    <div class="box-body col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Özellikler*</label>
                            <select name="properties[]" class="form-control select2" multiple="multiple"
                                    style="width: 100%;">
                                <?php foreach (get_room_properties() as $property) { ?>
                                <option value="<?php echo $property->id ?>"><?php echo $property->title ?></option>
                               <?php } ?>
                            </select></div>
                    </div>
                    <div class="box-body col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ekstra Servisler*</label>
                            <select name="extra_services[]" class="form-control select2" multiple="multiple"
                                    style="width: 100%;">
                                <option>Alabama</option>
                                <option>Alaska</option>
                                <option>California</option>
                                <option>Delaware</option>
                                <option>Tennessee</option>
                                <option>Texas</option>
                                <option>Washington</option>
                            </select></div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                        <a href="<?php echo base_url("room") ?>" type="submit" class="btn btn-danger">İptal</a>

                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
