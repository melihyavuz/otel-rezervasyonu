<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="<?php echo base_url("assets") ?>/plugins/select2/select2.full.min.js"></script>

<script>
    $(function () {
        $(".select2").select2();

        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('detail');
        //bootstrap WYSIHTML5 - text editor
    });
</script>