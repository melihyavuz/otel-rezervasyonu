<script src="<?php echo base_url("assets"); ?>/dist/js/third_party/bootstrap-toggle.min.js"></script>

<script>
    $(document).ready(function () {

        $('.toggle_check').bootstrapToggle();
        $('.toggle_check').change(function () {
            var isActive = $(this).prop('checked');
            var id=$(this).attr("dataID");
            var base_url= $('.base_url').text();
             $.post(
                 base_url+"room/isActiveSetter",{id:id,isActive:isActive},
                 function (response) {
                 }

             )
        })
    })

</script>
