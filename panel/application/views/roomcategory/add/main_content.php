
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <div class="box box-primary">

                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="<?php echo base_url("roomcategory/add") ?>" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Kategori Adı*</label>
                            <input type="text" class="form-control" name="title" placeholder="Kategori giriniz...">

                        </div>

                    </div>

                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                        <a href="<?php echo base_url("roomcategory") ?>" type="submit" class="btn btn-danger">İptal</a>

                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
