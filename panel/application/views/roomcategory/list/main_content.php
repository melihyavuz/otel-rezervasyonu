<?php  $page="roomcategory"; ?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <a href="<?php echo base_url("roomcategory/newPage") ?>" class="btn btn-sm btn-primary mb-10"> <i
                        class="fa fa-plus"></i>Ekle</a>
            <div class="box">

                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Başlık</th>
                            <th>Durum</th>
                            <th class="col-md-2">İşlemler</th>
                        </tr>
                        <tbody id="sortable" postUrl="roomcategory/rankOrder">
                        <?php foreach ($categories as $category) { ?>

                            <tr id="sortId-<?php echo $category->id; ?>" >
                                <td><?php echo $category->title ?></td>
                                <td>
                                    <input <?php echo ($category->isActive == 1) ? "checked" : ""; ?>
                                            dataID="<?php echo $category->id ?>" class="toggle_check"
                                            data-toggle="toggle" data-onstyle="success" data-offstyle="danger"
                                            data-on="Aktif" data-off="Pasif" data-size="mini" type="checkbox">
                                </td>
                                <td>
                                    <a href="<?php echo base_url("roomcategory/editPage/$category->id") ?>"> <i
                                                class="fa fa-edit" style="font-size:16px;"></i></a>
                                    <a class="removeBtn"
                                       dataURL="<?php echo base_url("roomcategory/delete/$category->id") ?>"> <i
                                                class="fa fa-fw fa-trash" style="font-size:16px;"></i></a></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>


</section>
