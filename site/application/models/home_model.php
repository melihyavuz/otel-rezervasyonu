<?php

class Home_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = "room";
    }

    public function get_all()
    {
        return $this->db->get("room")->result();
    }

    public function join()
    {
        $this->db->select('*');
        $this->db->from('room_image');
        $this->db->where('room_image.isActive', 1);

        $this->db->where('isCover', 1);

        $this->db->join('room', 'room_image.room_id = room.id');
        $query = $this->db->get();

        return $query->result();
    }

    public function join_filter($where = array())
    {


        if (implode($where) == 1) {

            $this->db->select('*');
            $this->db->from('room_image');
            $this->db->where('isCover', 1);
            $this->db->where('room.isActive', 1);
            $this->db->where('room_image.isActive', 1);

        } else {
            $this->db->select('*');
            $this->db->from('room_image');
            $this->db->where('isCover', 1);
            $this->db->where('room.isActive', 1);
            $this->db->where('room_image.isActive', 1);

            $this->db->where($where);

        }


        $this->db->join('room', 'room_image.room_id = room.id');
        $query = $this->db->get();

        return $query->result();
    }


}