<!doctype html>
<html lang="tr">
<head>
    <?php $this->load->view("includes/head"); ?>
    <?php $this->load->view("includes/include_style"); ?>
    <?php $this->load->view("room_list/page_style"); ?>

</head>
<body>
<?php $this->load->view("includes/header"); ?>
<?php $this->load->view("room_list/breadcrumb"); ?>
<?php $this->load->view("room_list/room_list"); ?>
<?php $this->load->view("room_list/main_content"); ?>


<?php $this->load->view("includes/footer"); ?>
<?php $this->load->view("room_list/page_script"); ?>

<?php $this->load->view("includes/include_script"); ?>


</body>
</html>