<!doctype html>
<html lang="tr">
<head>
    <?php $this->load->view("includes/head"); ?>
    <?php $this->load->view("includes/include_style"); ?>
    <?php $this->load->view("home/page_style"); ?>

</head>
<body>
<?php $this->load->view("includes/header"); ?>
<?php $this->load->view("home/slider"); ?>

<?php $this->load->view("home/room_list"); ?>
<?php $this->load->view("home/usp_list"); ?>
<?php $this->load->view("home/parallax"); ?>
<?php $this->load->view("home/gallery"); ?>
<?php $this->load->view("home/testimonials"); ?>


<?php $this->load->view("includes/footer"); ?>
<?php $this->load->view("includes/include_script"); ?>

</body>
</html>