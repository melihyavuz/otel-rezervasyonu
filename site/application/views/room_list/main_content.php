<section id="filter_list_image"  class="rooms mt50">

    <div  class="container">
        <div  class="row">
            <div class="col-sm-12">
                <h2 class="lined-heading"><span>Oda Listesi</span></h2>
            </div>
            <!-- Room -->
            <?php foreach ($rooms

                           as $room) { ?>
                <div    class="col-sm-4">
                    <div  class="room-thumb"><img  src="<?php echo "../panel/uploads/" . $room->img_id; ?>" alt="room 1"
                                                   class="img-responsive"/>
                        <div  class="mask">
                            <div class="main">
                                <h5><?php echo $room->title; ?></h5>
                                <div class="price"><?php echo $room->default_price; ?> ₺<span>a night</span></div>
                            </div>
                            <div class="content">
                                <p>
                                    <?php if (strlen($room->detail) < 150) {
                                        echo $room->detail;
                                    } else {
                                        $substr = substr($room->detail, '0', '140');
                                        echo $substr . "..." . "<a href=''>devamı için tıklayınız.</a>";

                                    } ?></p>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <ul class="list-unstyled">
                                            <?php $ArrProperties = explode(",", $room->room_properties);
                                            ?>

                                            <?php for ($i = 0;
                                                       $i < count($ArrProperties);
                                                       $i++) { ?>
                                                <?php if ($ArrProperties[$i] % 2 == 1) { ?>
                                                    <div class="col-xs-6 pl-8">
                                                        <?php if (!empty($ArrProperties[$i])) { ?>

                                                            <li>
                                                                <i class="fa fa-check-circle"></i> <?php echo get_properties_name($ArrProperties[$i]); ?>
                                                            </li>
                                                        <?php } else { ?>
                                                            <div class="col-md-12 pl-8">

                                                                <li>
                                                                    <i class="fa fa-check-circle"></i> <span>asd</span>
                                                                </li>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="col-xs-6 pl-8">


                                                        <?php if (!empty($ArrProperties[$i])) { ?>

                                                            <li>
                                                                <i class="fa fa-check-circle"></i> <?php echo get_properties_name($ArrProperties[$i]); ?>
                                                            </li>
                                                        <?php } else { ?>
                                                            <div class="col-md-12 pl-8">
                                                                <div class="w-235">
                                                                    <i class="fa fa-warning"></i> <span class="red"><b>Dikkat bu odaya özellik eklenmedi!!!</b></span>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>

                                    </div>


                                </div>
                                <a href="room-detail.html" class="btn btn-primary btn-block mt-10">Daha Fazla Oku</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>


        </div>

    </div>
</section>