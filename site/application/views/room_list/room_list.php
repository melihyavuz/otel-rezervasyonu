<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-pills" id="filters">

                <?php foreach ($categories as $category) { ?>
                    <li class="<?php echo ($category->id==1)?  "active":  "" ?>"><a  dataURL="<?php echo base_url("room_list/"); ?>" dataID="<?php echo $category->id; ?>"  href="#" data-filter="*"><?php echo $category->title;?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>

