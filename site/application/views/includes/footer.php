<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <h4>About SecondHome</h4>
                <p>Suspendisse sed sollicitudin nisl, at dignissim libero. Sed porta tincidunt ipsum, vel volutpat. <br>
                    <br>
                    Nunc ut fringilla urna. Cras vel adipiscing ipsum. Integer dignissim nisl eu lacus interdum facilisis. Aliquam erat volutpat. Nulla semper vitae felis vitae dapibus. </p>
            </div>
            <div class="col-md-3 col-sm-3">
                <h4>Recieve our newsletter</h4>
                <p>Suspendisse sed sollicitudin nisl, at dignissim libero. Sed porta tincidunt ipsum, vel volutpa!</p>
                <form role="form">
                    <div class="form-group">
                        <input name="newsletter" type="text" id="newsletter" value="" class="form-control" placeholder="Please enter your E-mailaddress">
                    </div>
                    <button type="submit" class="btn btn-lg btn-black btn-block">Submit</button>
                </form>
            </div>

            <div class="col-md-3 col-sm-3">
                <h4>Address</h4>
                <address>
                    <strong>SecondHome</strong><br>
                    Hocapasa Mahallesi Ebussuud Caddesi<br>
                    No: 19, Sirkeci<br>
                    <abbr title="Phone">P:</abbr> <a href="#">(212) 456-7890</a><br>
                    <abbr title="Email">E:</abbr> <a href="#">secondhomehostel@gmail.com</a><br>
                    <abbr title="Website">W:</abbr> <a href="#">www.hostelsh.com</a><br>
                </address>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-6"> &copy; 2015 SecondHome All Rights Reserved </div>
                <div class="col-xs-6 text-right">
                    <ul>
                        <li><a href="contact-02.html">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>