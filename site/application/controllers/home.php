<?php

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("home_model");

    }

    public function index()
    {
        $rooms = $this->home_model->join();
        $viewData = array("rooms" => $rooms);
        $this->load->view("home",$viewData);

    }



}