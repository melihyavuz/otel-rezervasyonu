<?php

class Room_list extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("roomcategories_model");
        $this->load->model("home_model");

    }

    public function index()
    {

        $categories = $this->roomcategories_model->get_all();
        $rooms = $this->home_model->join();

        $viewData = array(
            "categories" => $categories,
            "rooms" => $rooms
        );

        $this->load->view("room_list", $viewData);



    }
    public function filter(){
        $id=$this->input->post("id");
        $categories = $this->roomcategories_model->get_all();

        $rooms = $this->home_model->join_filter(array("room_type_id" => $id));
        $viewData = array(
            "categories" => $categories,
            "rooms" => $rooms
        );

        $this->load->view("room_list/main_content", $viewData);


    }

}